Hylaa-continuous repeatability package for 2020 ARCH-Comp Linear System Group, prepared by Stanley Bak.

Instructions:

Build the container
> docker build . -t hylaa_continuous


Run all measurements ((results printed to stdout; should match expected_stdout.txt)), takes 2 minutes:
> docker run hylaa_continuous


for heat3d, the max temp can be found by examinin the reach_data.txt file after executing, which is a gnuplot polygon file with one square for the temp at each step. This can also be plotted using the included gnuplot scripts in the heat3d folder, to help narrow the search for the maximum temperature / time.
