function example_hybrid_reach_ARCH20_rendezvous_SRA06()
% example_hybrid_reach_ARCH20_rendezvous_SRA06 - example of linear 
% reachability analysis from the ARCH20 friendly competition 
% (instance of rendevouz example). The guard intersections are calculated
% with Girard's method
%
% Syntax:  
%    example_hybrid_reach_ARCH20_rendezvous_SRA06
%
% Inputs:
%    no
%
% Outputs:
%    res - boolean 
%
% Example: 
%
% 
% Author:       Niklas Kochdumper
% Written:      20-March-2018
% Last update:  ---
% Last revision:---


%------------- BEGIN CODE --------------

% x_1 = v_x
% x_2 = v_y
% x_3 = s_x 
% x_4 = s_y
% x_5 = t



% Options -----------------------------------------------------------------

% debugging mode
options.debug = 0;

% initial set
R0 = zonotope([[-900; -400; 0; 0; 0],diag([25;25;0;0;0])]);

% initial set
params.x0=center(R0); % initial state for simulation
params.R0=R0; % initial state for reachability analysis

% other
params.startLoc = 1; % initial location
params.finalLoc = 0; % no final location
params.tStart=0; % start time
params.tFinal=300; % final time
options.intermediateOrder = 2;
options.originContained = 0;
options.timeStepLoc{1} = 2e-1;
options.timeStepLoc{2} = 2e-2;
options.timeStepLoc{3} = 2e-1;

options.zonotopeOrder=10; % zonotope order
options.polytopeOrder=3; % polytope order
options.taylorTerms=3;
options.reductionTechnique = 'girard';
options.isHyperplaneMap=0;
options.filterLength = [5,7];
options.compTimePoint = 0;


% specify hybrid automata
HA = rendezvous_SRA06(); % automatically converted from SpaceEx


% set input:
% get locations
loc = get(HA,'location');
nrOfLoc = length(loc);
for i=1:nrOfLoc
    params.uLoc{i} = 0; % input for simulation
    params.uLocTrans{i} = params.uLoc{i}; % input center for reachability analysis
    params.Uloc{i} = zonotope(0); % input deviation for reachability analysis
end




% Simulation --------------------------------------------------------------

% simulate hybrid automaton
HA = simulate(HA,params); 




% Reachability Analysis ---------------------------------------------------

%reachable set computations
warning('off','all')
tic

% first part
params.tFinal = 110;
options.guardIntersect = 'zonoGirard';
options.enclose = 1;

[HA1] = reach(HA,params,options);

Rcont1 = get(HA1,'continuousReachableSet');
Rcont1 = Rcont1.OT;
loc1 = get(HA1,'finalLocation');

% second part

params.tFinal = 190;
options.guardIntersect = 'nondetGuard';
options.enclose = 2;
options.zonotopeOrder = 2;
params.R0 = Rcont1{end}{end};
params.startLoc = loc1(end);

[HA2] = reach(HA,params,options);

Rcont2 = get(HA2,'continuousReachableSet');
Rcont2 = Rcont2.OT;
loc2 = get(HA2,'finalLocation');

tComp = toc;
disp(['computation time for spacecraft rendezvous: ',num2str(tComp)]);
warning('on','all')




% Verification ------------------------------------------------------------

tic
ind12 = find(loc1 == 2);
ind22 = find(loc2 == 2);
ind23 = find(loc2 == 3);
R2 = [Rcont1{ind12(1)},Rcont2{ind22(1)}];
R3 = Rcont2{ind23(1)};
verificationCollision = 1;
verificationVelocity = 1;
verificationLOS = 1;
spacecraft = interval([-0.1;-0.1],[0.1;0.1]);

% feasible velocity region as polytope
C = [0 0 1 0 0;0 0 -1 0 0;0 0 0 1 0;0 0 0 -1 0;0 0 1 1 0;0 0 1 -1 0;0 0 -1 1 0;0 0 -1 -1 0];
d = [3;3;3;3;4.2;4.2;4.2;4.2];

% line-of-sight as polytope
Cl = [-1 0 0 0 0;tan(pi/6) -1 0 0 0;tan(pi/6) 1 0 0 0];
dl = [100;0;0];

% passive mode -> check if the spacecraft was hit
for i = 1:length(R3)    
    temp = interval(R3{i});
    if isIntersecting(temp(1:2),spacecraft)
       verificationCollision = 0;
       break;
    end
end

% randezvous attempt -> check if spacecraft inside line-of-sight
for i = 2:length(R2)  

    temp = interval(Cl*R2{i})-dl;

    if any(supremum(temp) > 0)
       verificationLOS = 0;
       break;
    end
end

% randezvous attempt -> check if velocity inside feasible region
for i = 1:length(R2)  

    temp = interval(C*R2{i})-d;

    if any(supremum(temp) > 0)
       verificationVelocity = 0;
       break;
    end
end

tVer = toc;

verificationCollision
verificationVelocity
verificationLOS

disp(['computation time of verification: ',num2str(tVer)]);
disp(' ');
disp(['overall computation time: ',num2str(tVer + tComp)]);
disp(' ');




% Visualiztion ------------------------------------------------------------

warning('off','all')
figure 
hold on
box on
fill([-100,0,-100],[-60,0,60],'y','FaceAlpha',0.6,'EdgeColor','none');
options.projectedDimensions = [1 2];
options.plotType = {'b','m','g'};
plot(HA1,'reachableSet',options); %plot reachable set
plot(HA2,'reachableSet',options); %plot reachable set
plotFilled(params.R0,options.projectedDimensions,'w','EdgeColor','k'); %plot initial set
plot(HA,'simulation',options); % plot simulation
xlabel('s_x');
ylabel('s_y');

figure 
hold on
box on
fill([-3,-1.2,1.2,3,3,1.2,-1.2,-3],[-1.2,-3,-3,-1.2,1.2,3,3,1.2],'y','FaceAlpha',0.6,'EdgeColor','none');
options.projectedDimensions = [3 4];
options.plotType = {'b','m','g'};
plot(HA1,'reachableSet',options); %plot reachable set
plot(HA2,'reachableSet',options); %plot reachable set
plotFilled(params.R0,options.projectedDimensions,'w','EdgeColor','k'); %plot initial set
plot(HA,'simulation',options); % plot simulation
warning('on','all')
xlabel('v_x [m]');
ylabel('v_y [m/min]');




% Visited Locations -------------------------------------------------------

% get results
traj = get(HA,'trajectory');
locTraj = traj.loc;
setTraj = unique(locTraj)


%------------- END OF CODE --------------
