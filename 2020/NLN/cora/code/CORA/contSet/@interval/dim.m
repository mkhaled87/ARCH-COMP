function d = dim(obj)
% dim - returns the dimension the interval is a subset of
%
% Syntax:  
%    d = dim(obj)
%
% Inputs:
%    obj - interval object
%
% Outputs:
%    d - dimension of obj 
%
% Example: 
%    ---
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: none

% Author:       Mark Wetzlinger
% Written:      15-Sep-2019
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------

infi = infimum(obj); % equivalently: supremum(obj)
[rows, cols] = size(infi);
if rows == 1
    d = cols;
elseif cols == 1
    d = rows;
else
    error("Wrong input!");
end


%------------- END OF CODE --------------