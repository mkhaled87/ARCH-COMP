function exec_ARCH19_nonlinear(path)

    % create path where the results are saved
    path = fullfile(pwd,path);
    
    % spacecraft benchmark
    example_hybrid_reach_ARCH19_spacecraft();
    handles = get(groot, 'Children');
    figure(handles(1));
    print(fullfile(path,'spacecraft_1.png'),'-dpng');
    figure(handles(2));
    print(fullfile(path,'spacecraft_2.png'),'-dpng');
    close all
    
    % laub-loomis benchmark
    example_nonlinear_reach_ARCH19_laubLoomis();
    print(fullfile(path,'laubLoomis.png'),'-dpng');
    close all
    
    % quadrotor benchmark
    example_nonlinear_reach_ARCH19_quadrotor();
    print(fullfile(path,'quadrotor.png'),'-dpng');
    close all
    
    % van-der-pol benchmark
    example_nonlinear_reach_ARCH19_vanDerPol();
    print(fullfile(path,'vanDerPol.png'),'-dpng');
    close all
    
end