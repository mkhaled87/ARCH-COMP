function [E] = lplus(E1,E2,l)
% lplus - Computes the Minkowski sum of two ellipsoids such that resulting
% overapproximation given by E is tight in direction l
%
% Syntax:  
%    [E] = lplus(E1,E2,l)
%
% Inputs:
%    E1 - Ellipsoid object
%    E2 - Ellipsoid object 
%    l  - Direction
%
% Outputs:
%    E - Ellipsoid after addition of two ellipsoids
%
% Example: 
%    E1=ellipsoid([1 0; 0 1]);
%    E2=ellipsoid([1 1; 1 1]);
%    l =[1;0];
%    E =lplus(E1,E2,l);
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: -

% Author:       Matthias Althoff
% Written:      30-September-2006 
% Last update:  07-September-2007
%               05-January-2009
%               06-August-2010
%               01-February-2011
%               08-February-2011
%               18-November-2015
% Last revision:---

%------------- BEGIN CODE --------------
q = E1.q+E2.q;
Q1 = E1.Q;
Q2 = E2.Q;

Q = (sqrt(l'*Q1*l)+sqrt(l'*Q2*l))*(Q1/sqrt(l'*Q1*l)+Q2/sqrt(l'*Q2*l));
E = ellipsoid(Q,q);
%------------- END OF CODE --------------