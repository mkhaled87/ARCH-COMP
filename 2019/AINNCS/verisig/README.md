Verisig repeatability package for 2019 ARCH, prepared by Taylor Carpenter.

Instructions:

Build the container
> docker build . -t verisig

To run the Automatic Cruise Control (ACC) benchmark, run the following commands
> docker run -it verisig
> cd benchmarks/ACC
> ./benchmark_runner.py

To run the CartPole benchmark, run the following commands
> docker run -it verisig
> cd benchmarks/CartPole
> ./benchmark_runner.py