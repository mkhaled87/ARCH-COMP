function Hf=hessianTensor_quadrocopterControlledEq(x,u)



 Hf{1} = interval(sparse(15,15),sparse(15,15));

Hf{1}(8,4) = -cos(x(9))*sin(x(8));
Hf{1}(9,4) = -cos(x(8))*sin(x(9));
Hf{1}(7,5) = sin(x(7))*sin(x(9)) + cos(x(7))*cos(x(9))*sin(x(8));
Hf{1}(8,5) = cos(x(8))*cos(x(9))*sin(x(7));
Hf{1}(9,5) = - cos(x(7))*cos(x(9)) - sin(x(7))*sin(x(8))*sin(x(9));
Hf{1}(7,6) = cos(x(7))*sin(x(9)) - cos(x(9))*sin(x(7))*sin(x(8));
Hf{1}(8,6) = cos(x(7))*cos(x(8))*cos(x(9));
Hf{1}(9,6) = cos(x(9))*sin(x(7)) - cos(x(7))*sin(x(8))*sin(x(9));
Hf{1}(5,7) = sin(x(7))*sin(x(9)) + cos(x(7))*cos(x(9))*sin(x(8));
Hf{1}(6,7) = cos(x(7))*sin(x(9)) - cos(x(9))*sin(x(7))*sin(x(8));
Hf{1}(7,7) = x(5)*(cos(x(7))*sin(x(9)) - cos(x(9))*sin(x(7))*sin(x(8))) - x(6)*(sin(x(7))*sin(x(9)) + cos(x(7))*cos(x(9))*sin(x(8)));
Hf{1}(8,7) = x(5)*cos(x(7))*cos(x(8))*cos(x(9)) - x(6)*cos(x(8))*cos(x(9))*sin(x(7));
Hf{1}(9,7) = x(5)*(cos(x(9))*sin(x(7)) - cos(x(7))*sin(x(8))*sin(x(9))) + x(6)*(cos(x(7))*cos(x(9)) + sin(x(7))*sin(x(8))*sin(x(9)));
Hf{1}(4,8) = -cos(x(9))*sin(x(8));
Hf{1}(5,8) = cos(x(8))*cos(x(9))*sin(x(7));
Hf{1}(6,8) = cos(x(7))*cos(x(8))*cos(x(9));
Hf{1}(7,8) = x(5)*cos(x(7))*cos(x(8))*cos(x(9)) - x(6)*cos(x(8))*cos(x(9))*sin(x(7));
Hf{1}(8,8) = - x(4)*cos(x(8))*cos(x(9)) - x(6)*cos(x(7))*cos(x(9))*sin(x(8)) - x(5)*cos(x(9))*sin(x(7))*sin(x(8));
Hf{1}(9,8) = x(4)*sin(x(8))*sin(x(9)) - x(6)*cos(x(7))*cos(x(8))*sin(x(9)) - x(5)*cos(x(8))*sin(x(7))*sin(x(9));
Hf{1}(4,9) = -cos(x(8))*sin(x(9));
Hf{1}(5,9) = - cos(x(7))*cos(x(9)) - sin(x(7))*sin(x(8))*sin(x(9));
Hf{1}(6,9) = cos(x(9))*sin(x(7)) - cos(x(7))*sin(x(8))*sin(x(9));
Hf{1}(7,9) = x(5)*(cos(x(9))*sin(x(7)) - cos(x(7))*sin(x(8))*sin(x(9))) + x(6)*(cos(x(7))*cos(x(9)) + sin(x(7))*sin(x(8))*sin(x(9)));
Hf{1}(8,9) = x(4)*sin(x(8))*sin(x(9)) - x(6)*cos(x(7))*cos(x(8))*sin(x(9)) - x(5)*cos(x(8))*sin(x(7))*sin(x(9));
Hf{1}(9,9) = x(5)*(cos(x(7))*sin(x(9)) - cos(x(9))*sin(x(7))*sin(x(8))) - x(6)*(sin(x(7))*sin(x(9)) + cos(x(7))*cos(x(9))*sin(x(8))) - x(4)*cos(x(8))*cos(x(9));


 Hf{2} = interval(sparse(15,15),sparse(15,15));

Hf{2}(8,4) = -sin(x(8))*sin(x(9));
Hf{2}(9,4) = cos(x(8))*cos(x(9));
Hf{2}(7,5) = cos(x(7))*sin(x(8))*sin(x(9)) - cos(x(9))*sin(x(7));
Hf{2}(8,5) = cos(x(8))*sin(x(7))*sin(x(9));
Hf{2}(9,5) = cos(x(9))*sin(x(7))*sin(x(8)) - cos(x(7))*sin(x(9));
Hf{2}(7,6) = - cos(x(7))*cos(x(9)) - sin(x(7))*sin(x(8))*sin(x(9));
Hf{2}(8,6) = cos(x(7))*cos(x(8))*sin(x(9));
Hf{2}(9,6) = sin(x(7))*sin(x(9)) + cos(x(7))*cos(x(9))*sin(x(8));
Hf{2}(5,7) = cos(x(7))*sin(x(8))*sin(x(9)) - cos(x(9))*sin(x(7));
Hf{2}(6,7) = - cos(x(7))*cos(x(9)) - sin(x(7))*sin(x(8))*sin(x(9));
Hf{2}(7,7) = x(6)*(cos(x(9))*sin(x(7)) - cos(x(7))*sin(x(8))*sin(x(9))) - x(5)*(cos(x(7))*cos(x(9)) + sin(x(7))*sin(x(8))*sin(x(9)));
Hf{2}(8,7) = x(5)*cos(x(7))*cos(x(8))*sin(x(9)) - x(6)*cos(x(8))*sin(x(7))*sin(x(9));
Hf{2}(9,7) = x(5)*(sin(x(7))*sin(x(9)) + cos(x(7))*cos(x(9))*sin(x(8))) + x(6)*(cos(x(7))*sin(x(9)) - cos(x(9))*sin(x(7))*sin(x(8)));
Hf{2}(4,8) = -sin(x(8))*sin(x(9));
Hf{2}(5,8) = cos(x(8))*sin(x(7))*sin(x(9));
Hf{2}(6,8) = cos(x(7))*cos(x(8))*sin(x(9));
Hf{2}(7,8) = x(5)*cos(x(7))*cos(x(8))*sin(x(9)) - x(6)*cos(x(8))*sin(x(7))*sin(x(9));
Hf{2}(8,8) = - x(4)*cos(x(8))*sin(x(9)) - x(6)*cos(x(7))*sin(x(8))*sin(x(9)) - x(5)*sin(x(7))*sin(x(8))*sin(x(9));
Hf{2}(9,8) = x(6)*cos(x(7))*cos(x(8))*cos(x(9)) - x(4)*cos(x(9))*sin(x(8)) + x(5)*cos(x(8))*cos(x(9))*sin(x(7));
Hf{2}(4,9) = cos(x(8))*cos(x(9));
Hf{2}(5,9) = cos(x(9))*sin(x(7))*sin(x(8)) - cos(x(7))*sin(x(9));
Hf{2}(6,9) = sin(x(7))*sin(x(9)) + cos(x(7))*cos(x(9))*sin(x(8));
Hf{2}(7,9) = x(5)*(sin(x(7))*sin(x(9)) + cos(x(7))*cos(x(9))*sin(x(8))) + x(6)*(cos(x(7))*sin(x(9)) - cos(x(9))*sin(x(7))*sin(x(8)));
Hf{2}(8,9) = x(6)*cos(x(7))*cos(x(8))*cos(x(9)) - x(4)*cos(x(9))*sin(x(8)) + x(5)*cos(x(8))*cos(x(9))*sin(x(7));
Hf{2}(9,9) = x(6)*(cos(x(9))*sin(x(7)) - cos(x(7))*sin(x(8))*sin(x(9))) - x(5)*(cos(x(7))*cos(x(9)) + sin(x(7))*sin(x(8))*sin(x(9))) - x(4)*cos(x(8))*sin(x(9));


 Hf{3} = interval(sparse(15,15),sparse(15,15));

Hf{3}(8,4) = cos(x(8));
Hf{3}(7,5) = -cos(x(7))*cos(x(8));
Hf{3}(8,5) = sin(x(7))*sin(x(8));
Hf{3}(7,6) = cos(x(8))*sin(x(7));
Hf{3}(8,6) = cos(x(7))*sin(x(8));
Hf{3}(5,7) = -cos(x(7))*cos(x(8));
Hf{3}(6,7) = cos(x(8))*sin(x(7));
Hf{3}(7,7) = x(6)*cos(x(7))*cos(x(8)) + x(5)*cos(x(8))*sin(x(7));
Hf{3}(8,7) = x(5)*cos(x(7))*sin(x(8)) - x(6)*sin(x(7))*sin(x(8));
Hf{3}(4,8) = cos(x(8));
Hf{3}(5,8) = sin(x(7))*sin(x(8));
Hf{3}(6,8) = cos(x(7))*sin(x(8));
Hf{3}(7,8) = x(5)*cos(x(7))*sin(x(8)) - x(6)*sin(x(7))*sin(x(8));
Hf{3}(8,8) = x(6)*cos(x(7))*cos(x(8)) - x(4)*sin(x(8)) + x(5)*cos(x(8))*sin(x(7));


 Hf{4} = interval(sparse(15,15),sparse(15,15));

Hf{4}(12,5) = 1;
Hf{4}(11,6) = -1;
Hf{4}(8,8) = (981*sin(x(8)))/100;
Hf{4}(6,11) = -1;
Hf{4}(5,12) = 1;


 Hf{5} = interval(sparse(15,15),sparse(15,15));

Hf{5}(12,4) = -1;
Hf{5}(10,6) = 1;
Hf{5}(7,7) = -(981*cos(x(8))*sin(x(7)))/100;
Hf{5}(8,7) = -(981*cos(x(7))*sin(x(8)))/100;
Hf{5}(7,8) = -(981*cos(x(7))*sin(x(8)))/100;
Hf{5}(8,8) = -(981*cos(x(8))*sin(x(7)))/100;
Hf{5}(6,10) = 1;
Hf{5}(4,12) = -1;


 Hf{6} = interval(sparse(15,15),sparse(15,15));

Hf{6}(11,4) = 1;
Hf{6}(10,5) = -1;
Hf{6}(7,7) = -(981*cos(x(7))*cos(x(8)))/100;
Hf{6}(8,7) = (981*sin(x(7))*sin(x(8)))/100;
Hf{6}(7,8) = (981*sin(x(7))*sin(x(8)))/100;
Hf{6}(8,8) = -(981*cos(x(7))*cos(x(8)))/100;
Hf{6}(5,10) = -1;
Hf{6}(4,11) = 1;


 Hf{7} = interval(sparse(15,15),sparse(15,15));

Hf{7}(7,7) = - x(12)*cos(x(7))*tan(x(8)) - x(11)*sin(x(7))*tan(x(8));
Hf{7}(8,7) = x(11)*cos(x(7))*(tan(x(8))^2 + 1) - x(12)*sin(x(7))*(tan(x(8))^2 + 1);
Hf{7}(11,7) = cos(x(7))*tan(x(8));
Hf{7}(12,7) = -sin(x(7))*tan(x(8));
Hf{7}(7,8) = x(11)*cos(x(7))*(tan(x(8))^2 + 1) - x(12)*sin(x(7))*(tan(x(8))^2 + 1);
Hf{7}(8,8) = 2*x(12)*cos(x(7))*tan(x(8))*(tan(x(8))^2 + 1) + 2*x(11)*sin(x(7))*tan(x(8))*(tan(x(8))^2 + 1);
Hf{7}(11,8) = sin(x(7))*(tan(x(8))^2 + 1);
Hf{7}(12,8) = cos(x(7))*(tan(x(8))^2 + 1);
Hf{7}(7,11) = cos(x(7))*tan(x(8));
Hf{7}(8,11) = sin(x(7))*(tan(x(8))^2 + 1);
Hf{7}(7,12) = -sin(x(7))*tan(x(8));
Hf{7}(8,12) = cos(x(7))*(tan(x(8))^2 + 1);


 Hf{8} = interval(sparse(15,15),sparse(15,15));

Hf{8}(7,7) = x(12)*sin(x(7)) - x(11)*cos(x(7));
Hf{8}(11,7) = -sin(x(7));
Hf{8}(12,7) = -cos(x(7));
Hf{8}(7,11) = -sin(x(7));
Hf{8}(7,12) = -cos(x(7));


 Hf{9} = interval(sparse(15,15),sparse(15,15));

Hf{9}(7,7) = - (x(12)*cos(x(7)))/cos(x(8)) - (x(11)*sin(x(7)))/cos(x(8));
Hf{9}(8,7) = (x(11)*cos(x(7))*sin(x(8)))/cos(x(8))^2 - (x(12)*sin(x(7))*sin(x(8)))/cos(x(8))^2;
Hf{9}(11,7) = cos(x(7))/cos(x(8));
Hf{9}(12,7) = -sin(x(7))/cos(x(8));
Hf{9}(7,8) = (x(11)*cos(x(7))*sin(x(8)))/cos(x(8))^2 - (x(12)*sin(x(7))*sin(x(8)))/cos(x(8))^2;
Hf{9}(8,8) = (x(12)*cos(x(7)))/cos(x(8)) + (x(11)*sin(x(7)))/cos(x(8)) + (2*x(12)*cos(x(7))*sin(x(8))^2)/cos(x(8))^3 + (2*x(11)*sin(x(7))*sin(x(8))^2)/cos(x(8))^3;
Hf{9}(11,8) = (sin(x(7))*sin(x(8)))/cos(x(8))^2;
Hf{9}(12,8) = (cos(x(7))*sin(x(8)))/cos(x(8))^2;
Hf{9}(7,11) = cos(x(7))/cos(x(8));
Hf{9}(8,11) = (sin(x(7))*sin(x(8)))/cos(x(8))^2;
Hf{9}(7,12) = -sin(x(7))/cos(x(8));
Hf{9}(8,12) = (cos(x(7))*sin(x(8)))/cos(x(8))^2;


 Hf{10} = interval(sparse(15,15),sparse(15,15));

Hf{10}(12,11) = -25/27;
Hf{10}(11,12) = -25/27;


 Hf{11} = interval(sparse(15,15),sparse(15,15));

Hf{11}(12,10) = 25/27;
Hf{11}(10,12) = 25/27;


 Hf{12} = interval(sparse(15,15),sparse(15,15));

