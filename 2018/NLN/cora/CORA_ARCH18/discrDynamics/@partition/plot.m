function plot(Obj,varargin)
% Purpose:  plot state space partition
% Inputs:
% Obj:      Partition object
% optional 2nd argument: indices of segments to be visualised
%
% Output: none
% Tested:   1.8.17,AP
% TODO: visualise an arbitrary dimension

if nargin ==1
    Ints = segmentInterval(Obj);
else % desired segments
    Ints = segmentInterval(Obj,varargin{1});
end

if isempty(Ints)||(length(Obj.nrOfSegments)==1)
    disp('Partition is empty or of too low dimension')
    return
end

for iCell=1:length(Ints)
    IH=Ints{iCell};
    plot(IH,[1 2],'Color',[.8 .8 .8]);
    hold on
end
