function [Obj]=Reset(Obj)
% DEPRECATED; actualSegmentNr NO LONGER A FIELD OF PARTITION OBJECT
% Purpose:  reset segment nr
% Pre:      partition object
% Post:     partition object
% Built:    16.08.07,MA

% Raise segment number
Obj.actualSegmentNr=0;