function [automaton] = DummyMerge(instances)
%   If there is only 1 base component, find  and return it.
%   Otherwise crashes.

index = 1;
listOfVar = instances{index}.listOfVar;

while(instances{index}.isNetwork)
    children = instances{index}.children;
    index = children(1);
end

automaton = instances{index};
automaton.listOfVar = listOfVar;

end

