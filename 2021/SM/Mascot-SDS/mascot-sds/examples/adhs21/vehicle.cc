/*
 * vehicle.cc
 *
 *  created on: 16.10.2019
 *      author: kaushik
 */

/*
 * dubins vehicle (sampled time) with bounded external noise
 *
 */

#include <array>
#include <iostream>
#include <math.h>
#include <float.h>

#include "boost/lexical_cast.hpp"
#include "cuddObj.hh"

#include "SymbolicSet.hh"
// #include "SymbolicModelGrowthBound.hh"
#include "SymbolicModelMonotonic.hh"

#include "TicToc.hh"
#include "RungeKutta4.hh"
#include "FixedPointGR1.hh"

/* state space dim */
#define sDIM 4 /* 3 states + 1 flag */
#define iDIM 1

#ifdef _OPENMP // Check if compiler flag -fopenmp was active
    #define parallel 1
#else // Single threaded version
    #define parallel 0
#endif

/* data types for the ode solver */
typedef std::array<double,sDIM> state_type;
typedef std::array<double,iDIM> input_type;
typedef std::array<double,2> state_xy_dim_type;

/****************************************************************************/
/* main computation
 *
 *  Command line arguments:
 *  argv[1]: State space discretization for dimension 0: eta[0]
 *  argv[2]: State space discretization for dimension 1: eta[1]
 *  argv[3]: State space discretization for dimension 2: eta[2]
 *  argv[4]: (Optional) Name of the logfile, default="vehicle.log" (if one of this name doesn't exist, then create one, and if it exists then append to that existing file)
 *  argv[5]: (Optional) Verbosity: 0-2 (Default 0)
 *  argv[6]: (Optional) Read from file: 1 (logical true) or 0 (logical false); default is 0.
 *  argv[7]: (Optional) Warm-start the under-approximation using the outcome of the over-approximation: 1 (logical true) or 0 (logical false)
 *  argv[8]: (Optional) Name of the folder where the controllers and the synthesis data are stored, default="./data/" (if one of this name doesn't exist, then create one, and if it exists then overwrite the contents)
 */
/****************************************************************************/
int main(int argc, char** argv) {
    /* sanity check */
    if (argc<4) {
        std::ostringstream os;
        os << "Error: vehicle: too few arguments to run switch from command line.";
        throw std::invalid_argument(os.str().c_str());
    }
    double eta[sDIM];
    eta[0] = boost::lexical_cast<double>(argv[1]);
    eta[1] = boost::lexical_cast<double>(argv[2]);
    eta[2] = boost::lexical_cast<double>(argv[3]);
    eta[3] = 1.0;
    const char* logfile; /* name of the log file */
    int verbose=0; /* verbose = 0: no verbosity, 1: print messages on the terminal, 2: save intermediate sets computed in the fixed point */
    int RFF=0; /* read transitions from file flag */
    int warm_start_under_approx=0; /* warm-start the under-approximation from the outcome of the over-approximation */
    const char* data_path; /* name of the folder where the controllers and the other synthesis related data will be stored */
    if (argc>=5) {
        logfile=argv[4];
        std::freopen(logfile, "a", stderr);
    } else {
        /* create a new log file */
        logfile="vehicle.log";
        std::freopen(logfile, "a", stderr);
    }
    std::cout << "\n\n ******************\n";
    std::cout << "Starting controller synthesis for the vehicle example.\n\n";
//    std::time_t time = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
//    std::clog << "Time: " << std::ctime(&time) << '\n';
//    std::clog << "eta = [" << eta[0] << ", " << eta[1] << ", " << eta[2] << "]" << '\n';
    if (argc>=6) {
        verbose=boost::lexical_cast<int>(argv[5]);
    }
    if (argc>=7) {
        RFF=boost::lexical_cast<int>(argv[6]);
    }
    if (argc>=8) {
        warm_start_under_approx=boost::lexical_cast<int>(argv[7]);
    }
    if (argc>=9) {
        data_path=argv[8];
    } else {
        data_path = "data";
    }
    helper::checkMakeDir(data_path);
    
//    std::clog << "Verbosity: " << verbose << '\n';
//    if (parallel==1) {
//        std::clog << "Parallel GR1: ON" << '\n';
//    } else {
//        std::clog << "Parallel GR1: OFF" << '\n';
//    }
//    if (RFF==1) {
//        std::clog << "Read from file: TRUE" << '\n';
//    } else {
//        std::clog << "Read from file: FALSE" << '\n';
//    }
//    if (warm_start_under_approx==1) {
//        std::clog << "Warm-start under-approximation: TRUE" << '\n';
//    } else {
//        std::clog << "Warm-start under-approximation: FALSE" << '\n';
//    }
    /* ****** the system parameters ********* */
    /* sampling time */
    const double tau = 0.1;
    /* upper bounds on the noise which is considered to be centered around the origin */
    const state_type W_ub = {0.06,0.06,0.06,0.0};
    const double rad_reach_boolean_dim = 0.25; /* the radius of the reachable set from the state dimensions 4*/
    const double eps = 10e-6; /* some small number for tolerance of numerical errors */
    /* velocity */
    const double V = 1;
    double offset[sDIM] = {-0.6,-0.2,0.0,0.0};
    /* there is an intentional overlap between the two ends in the 3rd dimension to allow out-of-bound behavior */
    /* IMPORTANT: the overlap amount is ad-hoc and needs to be updated with eta */
    /* lower bounds of the hyper rectangle */
    double lb[sDIM]={-0.6,-1.2,-M_PI-0.46,0.0};
    /* upper bounds of the hyper rectangle */
    double ub[sDIM]={0.96,1.98,M_PI+0.46,1.0};
    /* input space parameters */
    double ilb[iDIM]={-20.0};
    double iub[iDIM]={20.0};
    double ieta[iDIM]={2.0};

    /* goal states */
    /* add inner approximation of P={ x | H x<= h } form state space */
    /* office */
    double H1[4*sDIM]={-1, 0, 0, 0,
        1, 0, 0, 0,
        0,-1, 0, 0,
        0, 1, 0, 0,
    };
    double g1[6] = {-0.20,0.80,-1.20,1.80};
    /* kitchen */
    double H2[4*sDIM]={-1, 0, 0, 0,
    1, 0, 0, 0,
    0,-1, 0, 0,
    0, 1, 0, 0,};
    double g2[4] = {-0.20-offset[0],0.80+offset[0],0.70-offset[1],-0.10+offset[1]};
    /* dynamic obstacle: closed door */
    double HDO[6*sDIM]={-1, 0, 0, 0,
        1, 0, 0, 0,
        0,-1, 0, 0,
        0, 1, 0, 0,
        0, 0, 0, -1,
        0, 0, 0, 1
    };
    double ho3[6] = {0-offset[0],1.56+offset[0],-0.85-offset[1],0.9+offset[1],0.49-offset[2],0.49+offset[2]};
    /* door opens */
    double HD[2*sDIM]={0, 0, 0, -1,
        0, 0, 0, 1};
    double a2[2] = {-0.51,1.49};

    /* returns true if [lb2,ub2] is included in [lb1,ub1] */
    auto check_inclusion = [&](state_xy_dim_type lb1, state_xy_dim_type ub1, state_xy_dim_type lb2, state_xy_dim_type ub2) -> bool {
        bool is_included=true; /* boolean flag which is positive if there is an intersection */
        /* if there is no intersection in either the first or the second state dimension, then there is no intersection */
        for (int i=0; i<2; i++) {
            if ((lb1[i]-lb2[i]>eps) || (ub2[i]-ub1[i]>eps)) {
                is_included=false;
                break;
            }
        }
        return is_included;
    };

    auto decomposition = [&](input_type &u, state_type &z1, state_type &z2) -> state_type {
        /* when the boolean flags are 2 then ignore (just make a self loop) */
        if ((abs(0.5*(z1[3]+z2[3])-2.0)<=eps) || (abs(0.5*(z1[4]+z2[4])-2.0)<=eps)) {
            state_type y=z1;
            return y;
        }
        /* first convert z1[2] to be within -pi and +pi */
        if (z1[2]>M_PI) {
            double rem = fmod(z1[2],M_PI);
            z1[2] = -M_PI + rem;
        }
        if (z1[2]<-M_PI) {
            double rem = fmod(-z1[2],M_PI);
            z1[2] = M_PI - rem;
        }
        auto rhs0 = [&](double x3, input_type& u) -> double {
            double r;
            if (u[0]!=0) {
                r = V*sin(x3+u[0]*tau)/u[0] - V*sin(x3)/u[0];
            } else {
                r = V*cos(x3)*tau;
            }
            return r;
        };
        auto rhs1 = [&](double x3, input_type& u) -> double {
            double r;
            if (u[0]!=0) {
                r = - V*cos(x3+u[0]*tau)/u[0] + V*cos(x3)/u[0];
            } else {
                r = - V*sin(x3)*tau;
            }
            return r;
        };
        std::vector<double> list0, list1;
        list0.push_back(z1[0]+rhs0(z1[2],u));
        list0.push_back(z1[0]+rhs0(z2[2],u));
        list0.push_back(z2[0]+rhs0(z1[2],u));
        list0.push_back(z2[0]+rhs0(z2[2],u));
        
        list1.push_back(z1[1]+rhs1(z1[2],u));
        list1.push_back(z1[1]+rhs1(z2[2],u));
        list1.push_back(z2[1]+rhs1(z1[2],u));
        list1.push_back(z2[1]+rhs1(z2[2],u));
        if (u[0]!=0) {
            double alpha0 = atan2((cos(u[0]*tau)-1),sin(u[0]*tau));
            if ((alpha0>=z1[2] && alpha0<=z2[2]) ||
                (alpha0<=z1[2] && alpha0>=z2[2])) {
                list0.push_back(z1[0]+rhs0(alpha0,u));
            }
            double alpha1 = atan2(sin(u[0]*tau),(1-cos(u[0]*tau)));
            if ((alpha1>=z1[2] && alpha1<=z2[2]) ||
                (alpha1<=z1[2] && alpha1>=z2[2])) {
                list1.push_back(z1[1]+rhs1(alpha1,u));
            }
        } else {
            double z2_min = std::min(z1[2],z2[2]);
            double z2_max = std::max(z1[2],z2[2]);
            if (0>=z2_min && 0<=z2_max) {
                list0.push_back(z1[0]+V*tau);
                list1.push_back(z1[1]);
            }
            if ((-M_PI>=z2_min && -M_PI<=z2_max) ||
                (M_PI>=z2_min && M_PI<=z2_max)) {
                list0.push_back(z1[0]-V*tau);
                list1.push_back(z1[1]);
            }
            /* extremum occurs for cos(x[2])=0 */
            if (M_PI/2>=z2_min && M_PI/2<=z2_max) {
                list0.push_back(z1[0]);
                list1.push_back(z1[1]-V*tau);
            }
            if (-M_PI/2>=z2_min && -M_PI/2<=z2_max) {
                list0.push_back(z1[0]);
                list1.push_back(z1[1]+V*tau);
            }
        }
        state_type y;
        if (z1[0]<z2[0]) { /* z1 is the lower bound and z2 is the upper bound */
            /* return the lower left corner of the reachable set approximation */
            y[0] = *std::min_element(std::begin(list0),std::end(list0));
            y[1] = *std::min_element(std::begin(list1),std::end(list1));
            y[2] = z1[2] + u[0]*tau;
        } else { /* z1 is the upper bound and z2 is the lower bound */
            /* return the upper right corner of the reachable set approximation */
            y[0] = *std::max_element(std::begin(list0),std::end(list0));
            y[1] = *std::max_element(std::begin(list1),std::end(list1));
            y[2] = z1[2] + u[0]*tau;
        }
        /* the X-Y dimension of the corners of the current state */
        state_xy_dim_type z1_xy_dim, z2_xy_dim;
        z1_xy_dim[0]=z1[0];
        z1_xy_dim[1]=z1[1];
        z2_xy_dim[0]=z2[0];
        z2_xy_dim[1]=z2[1];
        if (abs(z1[3]+z2[3])<=eps) {
            /* if the door is closed, it may open or close in the next time step */
            if (z1[0]<z2[0]) { /* z1 is the lower bound and z2 is the upper bound */
                /* return the lower left corner of the reachable set approximation */
                y[3] = 0.5-rad_reach_boolean_dim;
            } else { /* z1 is the upper bound and z2 is the lower bound */
                /* return the upper right corner of the reachable set approximation */
                y[3] = 0.5+rad_reach_boolean_dim;
            }
        } else if (abs(0.5*(z1[3]+z2[3])-1.0)<=eps) {
            /* the hyperrectangle representing the location of the kitchen */
            state_xy_dim_type lb2, ub2;
            lb2[0]=-g2[0];
            lb2[1]=-g2[2];
            ub2[0]=g2[1];
            ub2[1]=g2[3];
            /* when the door opens, it remains open until the robot reaches the kitchen */
            if (z1[0]<z2[0]) { /* z1 is the lower bound and z2 is the upper bound */
                if (check_inclusion(lb2,ub2,z1_xy_dim,z2_xy_dim)) {
                    y[3]=0.0-rad_reach_boolean_dim;
                } else {
                    y[3]=1.0-rad_reach_boolean_dim;
                }
            } else { /* z1 is the upper bound and z2 is the lower bound */
                /* return the upper right corner of the reachable set approximation */
                if ( check_inclusion(lb2,ub2,z2_xy_dim,z1_xy_dim)) {
                    y[3]=0.0+rad_reach_boolean_dim;
                } else {
                    y[3]=1.0+rad_reach_boolean_dim;
                }
            }
        }
        return y;
    };
    
    auto complete_data_path = [&](const char* filename) -> char* {
        std::string path=data_path;
        static char full_data_path[50];
        size_t Length;
        path+="/";
        path+=filename;
        Length=path.copy(full_data_path, path.length() + 1);
        full_data_path[Length] = '\0';
        return full_data_path;
    };
    
    /* to measure time */
    TicToc timer;
    /* there is one unique manager to organize the bdd variables */
    Cudd mgr;
    /****************************************************************************/
    /* construct SymbolicSet for the state space */
    /****************************************************************************/
    /* setup the workspace of the synthesis problem and the uniform grid */
    /* the varaibles lb, ub and eta are loaded from input.hh */
    scots::SymbolicSet ss(mgr,sDIM,lb,ub,eta);
    ss.addGridPoints();
//    /* debug */
//    ss.writeToFile("X.bdd");
    /****************************************************************************/
    /* construct SymbolicSet for the input space */
    /****************************************************************************/
    /* the varaibles ilb, iub and ieta are loaded from input.hh */
    scots::SymbolicSet is(mgr,iDIM,ilb,iub,ieta);
    is.addGridPoints();
    
    /****************************************************************************/
    /* setup class for symbolic model computation */
    /****************************************************************************/
    scots::SymbolicSet sspost(ss,1); /* create state space for post variables */
    scots::SymbolicModelMonotonic<state_type,input_type> abstraction(&ss, &is, &sspost);
    if (RFF==1) {
        std::cout << "Reading transition relations from file." << '\n';
        scots::SymbolicSet mt(mgr,complete_data_path("maybeTransitions.bdd"));
        scots::SymbolicSet st(mgr,complete_data_path("sureTransitions.bdd"));
        abstraction.setTransitionRelations(mt.getSymbolicSet(),st.getSymbolicSet());
        /* get the number of elements in the transition relation */
        std::cout << std::endl << "Number of elements in the maybe transition relation: " << abstraction.getSizeMaybe() << std::endl;
        std::cout << std::endl << "Number of elements in the sure transition relation: " << abstraction.getSizeSure() << std::endl;
    } else {
        /* compute the transition relation */
        std::cout << "Starting abstraction computation.\n";
        timer.tic();
        abstraction.computeTransitions(decomposition, W_ub);
        std::cout << std::endl;
        double time_abstraction = timer.toc();
        std::cout << "Time taken by the abstraction computation: " << time_abstraction << "s.\n";
        /* get the number of elements in the transition relation */
        std::cout << std::endl << "Number of elements in the maybe transition relation: " << abstraction.getSizeMaybe() << std::endl;
        std::cout << std::endl << "Number of elements in the sure transition relation: " << abstraction.getSizeSure() << std::endl;
        /* save transition relations */
        (abstraction.getMaybeTransitions()).writeToFile(complete_data_path("maybeTransitions.bdd"));
        (abstraction.getSureTransitions()).writeToFile(complete_data_path("sureTransitions.bdd"));
    }
    
    /****************************************************************************/
    /* we continue with the controller synthesis for
     *    GF A1 -> GF G1 & GF G2
     */
    /****************************************************************************/
    /* construct an underapproximating SymbolicSet for the guarantees (it is a subset of the state space)  */
    scots::SymbolicSet G1(ss), G2(ss);
    /* h is loaded from input.hh */
    G1.addPolytope(4,H1,g1, scots::INNER);
    G2.addPolytope(4,H2,g2, scots::INNER);
    // std::cout << "Target set details:" << std::endl;
    G1.writeToFile(complete_data_path("G1.bdd"));
    G2.writeToFile(complete_data_path("G2.bdd"));
    std::cout << "Guarantee sets wrote to files." << std::endl;
    
    /* construct an over-approximation of the avoid set */
    scots::SymbolicSet avoid_over(ss);
    avoid_over.addPolytope(6,HDO,ho3,scots::OUTER);
    avoid_over.writeToFile(complete_data_path("vehicle_avoid_over.bdd"));
    std::cout << "Avoid set wrote to file." << std::endl;
    
    /* construct an over-approximation of the assumptions */
    scots::SymbolicSet A1(ss);
    A1.addPolytope(2,HD,a2,scots::OUTER);
    A1.writeToFile(complete_data_path("A1.bdd"));
    std::cout << "Assumption sets wrote to file." << std::endl;
    
    /* we setup a fixed point object to compute reach and stay controller */
    scots::FixedPointGR1 fp(&abstraction);
    std::cout << "FixedPoint initialized." << '\n';
    
    std::vector<BDD> A;
    std::vector<BDD> G;
    A = {A1.getSymbolicSet()};
    G = {G1.getSymbolicSet(),G2.getSymbolicSet()};
    /* trivial spec */
//    /****************************************************************************/
//    /* Over-approximation of almost sure winning region */
//    /****************************************************************************/
//    std::cout << "\n\n Starting computation of over-approximation of the a.s. controller.\n";
//    timer.tic();
//    std::vector<BDD> CO = fp.GR1("over",A,G,mgr.bddOne(),avoid_over.getSymbolicSet(),verbose);
//    double time_over_approx = timer.toc();
//    scots::SymbolicSet controller_over_0(ss,is), controller_over_1(ss,is);
//    controller_over_0.setSymbolicSet(CO[0]);
//    controller_over_1.setSymbolicSet(CO[1]);
//    controller_over_0.writeToFile(complete_data_path("vehicle_controller_over_0.bdd"));
//    controller_over_1.writeToFile(complete_data_path("vehicle_controller_over_1.bdd"));
//    std::clog << "Time taken by the over-approximation computation: " << time_over_approx << "s.\n";
//    std::clog << "Number of states in the over-approximation: " << CO[0].ExistAbstract(fp.getCubeInput()).CountMinterm(ss.getNVars()) << std::endl;
    /****************************************************************************/
    /* Under-approximation of almost sure winning region */
    /****************************************************************************/
    BDD initial_seed=mgr.bddOne();
//    if (warm_start_under_approx==1) {
//        std::cout << "\n\n Starting computation of under-approximation of the a.s. controller (w/ initial seed).\n";
//        initial_seed=mgr.bddZero();
//        for (size_t i=0; i<CO.size(); i++) {
//            initial_seed |= CO[i];
//        }
//    } else {
//        std::cout << "\n\n Starting computation of under-approximation of the a.s. controller (w/o initial seed).\n";
//        initial_seed=mgr.bddOne();
//    }
    timer.tic();
    std::vector<BDD> CU = fp.GR1("under",A,G,initial_seed,avoid_over.getSymbolicSet(),verbose);
    double time_under_approx = timer.toc();
    scots::SymbolicSet controller_under_0(ss,is), controller_under_1(ss,is);
    controller_under_0.setSymbolicSet(CU[0]);
    controller_under_1.setSymbolicSet(CU[1]);
    controller_under_0.writeToFile(complete_data_path("vehicle_controller_under_0.bdd"));
    controller_under_1.writeToFile(complete_data_path("vehicle_controller_under_1.bdd"));
    std::clog << "Synthesis time for the vehicle example: " << time_under_approx << "s.\n";
    std::cout << "Number of states in the under-approximation: " << CU[0].ExistAbstract(fp.getCubeInput()).CountMinterm(ss.getNVars()) << std::endl;
//    std::clog << "Absolute volume of the error region: " << (CO[0].ExistAbstract(fp.getCubeInput()).CountMinterm(ss.getNVars()) - CU[0].ExistAbstract(fp.getCubeInput()).CountMinterm(ss.getNVars()))*eta[0]*eta[1]*eta[2] << std::endl;
///****************************************************************************/
//    /* Under-approximation of the worst-case winning region */
//    /****************************************************************************/
//    std::cout << "\n\n Starting computation of under-approximation of the worst case controller.\n";
//    timer.tic();
//    std::vector<BDD> CWC = fp.GR1("wc",A,G,controller_over_0.getSymbolicSet(),avoid_over.getSymbolicSet(),verbose);
//    double time_wc = timer.toc();
//    scots::SymbolicSet controller_wc_0(ss,is), controller_wc_1(ss,is);
//    controller_wc_0.setSymbolicSet(CWC[0]);
//    controller_wc_1.setSymbolicSet(CWC[1]);
//    controller_wc_0.writeToFile(complete_data_path("vehicle_controller_wc_0.bdd"));
//    controller_wc_1.writeToFile(complete_data_path("vehicle_controller_wc_1.bdd"));
//    std::clog << "Time taken by the worst case winning region computation: " << time_wc << "s.\n";
//    std::clog << "Number of states in the worst-case winning region: " << CWC[0].ExistAbstract(fp.getCubeInput()).CountMinterm(ss.getNVars()) << std::endl;

    return 1;
}
