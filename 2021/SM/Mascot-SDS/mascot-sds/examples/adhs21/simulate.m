%
% simulate.m
%
% created on: 09.10.2015
%     author: rungger (vehicle.m)
%     modified by: kaushik
%
%
% you need to run ./vehicle binary first 
%
% so that the file: vehicle_controller_under.bdd is created
%

% function simulate(loc_data,T,bound)
function simulate(varargin)
    clear set
    close all
    addpath(genpath('../../mfiles'))
    
    % default parameters
    loc_data = 'data_abstraction_1'; % location where controllers are stored
    T = 500; % simulation horizon 
    bound = 500; % bound on the maximum number of opening of the door
    prob_door=0.05; % the probability with which the door opens in the next time step
    
    % optional values
    for ii=1:nargin
        if (strcmp(varargin{ii},'Location'))
            loc_data=varargin{ii+1};
        elseif (strcmp(varargin{ii},'Horizon'))
            T=varargin{ii+1};
            if ischar(T)
                T=str2double(T); % the simulation horizon
            end
        elseif (strcmp(varargin{ii},'MaxDoorOpenCount'))
            bound=varargin{ii+1};
            if ischar(bound)
                bound=str2double(bound); % bound on the number of times the door may open
            end
        elseif (strcmp(varargin{ii},'ProbabilityDoorOpening'))
            prob_door=varargin{ii+1};
            if ischar(prob_door)
                prob_door=str2double(prob_door); % bound on the number of times the door may open
            end
        end
    end
    
    font = 11;
    
    
    % **** the system ****
    % state space bounds 
    lb = [-0.6;-1.2];
    ub = [0.96;1.98];
    % parameters
    W_ub=[0.06,0.06,0.06,0];
    tau=0.1;
    V = 1;
    % location of office
    office=[0.2,1.2,0.6,0.6];
    kitchen=[-0.4,-0.9,0.6,0.6];
    % prepare the figure window
    figure;
    axis([lb(1) ub(1) lb(2) ub(2)]);
    hold on
    
    % plot the location of the office and the kitchen
    rectangle('Position',office,'EdgeColor', 'b', 'LineWidth', 1.5);
    rectangle('Position',kitchen, 'EdgeColor', 'g', 'LineWidth', 2);
    % label the initial state, target, and obstacles
    text(office(1)+0.5*office(3),office(2)+0.5*office(4),'Office','interpreter','latex','FontSize',font);
    text(kitchen(1)+0.5*kitchen(3),kitchen(2)+0.5*kitchen(4),'Kitchen','interpreter','latex','FontSize',font);
    
    % prepare the location of the door
    door = [-0.6, 0.65, 1.56, 0.05];
    dHandle = rectangle('Position', door, 'EdgeColor', 'k', 'LineWidth', 1, 'LineStyle', '--');
    dTextHandle = text(-0.4,0.5,'Door','interpreter','latex','FontSize',font);

    % simulation
    % initial state
    x0=[0.2,0.5,-0.300000000000000,0];
    door_open_count=0; % actual number of opening of door
    
    % load the goals
%     G1 = SymbolicSet('G1.bdd','projection',[1 2]);
    G1 = SymbolicSet([loc_data '/G1.bdd']);
    G2 = SymbolicSet([loc_data '/G2.bdd']);

    % load the symbolic set containing the controller
    C0=SymbolicSet([loc_data '/vehicle_controller_under_0.bdd']);
    C1=SymbolicSet([loc_data '/vehicle_controller_under_1.bdd']);
    
    % simulate T time-steps
    y=x0;
    v=[];
    CurCont=0; % index of the current controller
    History=zeros(T,4); % history of the atomic propositions. Columns: 1-req, 2-door open, 3-robot serving req., 4-robot in kitchen
    for t=1:T
        disp(t)
        % switch the controller upon reaching the current goal
        if (CurCont==0 ...
            && atOffice(y(end,:)))
            CurCont = 1;
            History(t,3)=1;
        elseif (CurCont==1 ...
                 && atKitchen(y(end,:)))
            CurCont = 0;
            History(t,4)=1;
        end
        if (CurCont==0)
            u=C0.getInputs(y(end,:));
        else
            u=C1.getInputs(y(end,:));
        end
        if (y(end,4)==1)
            History(t,2)=1;
        end

        v=[v; u(1,:)];
        x = vehicle(y(end,:),v(end,:)); 

        y=[y; x(end,:)];
        plot(y(end-1:end,1),y(end-1:end,2),'r.-');
        h1 = plot(y(end,1),y(end,2),'ko','MarkerSize',10);

        pause(0.05)
        delete(h1);
        
    end

    savefig('traj_stochastic');
    save('simulation_history_0.mat','History', 'y');
    
    function status = atOffice(x)
        status = G1.isElement(x);
    end

    function status = atKitchen(x)
        status = G2.isElement(x);
    end

    function xn = vehicle(x,u)
        if (x(3)>pi)
            x(3) = -pi + mod(x(3),pi);
        elseif (x(3)<-pi)
            x(3) = pi - mod(-x(3),pi);
        end
        % uniform random noise
        w = 2*W_ub(1:3).*(rand(1,3))' - W_ub(1:3);
        % worst case noise
    %         w = wmax;
        xn = zeros(size(x));
        if (u~=0)
            xn(1) = x(1) + V*sin(x(3)+u*tau)/u - V*sin(x(3))/u + w(1);
            xn(2) = x(2) - V*cos(x(3)+u*tau)/u + V*cos(x(3))/u + w(2);
            xn(3) = x(3) + u*tau + w(3);
        else
            xn(1) = x(1) + V*cos(x(3))*tau + w(1);
            xn(2) = x(2) - V*sin(x(3))*tau + w(2);
            xn(3) = x(3) + w(3);
        end
        
        if (x(4)==0.0 && door_open_count<bound) % if the door is closed and bound is not reached, it may open in the next time step with
        % some probability
            r = rand;
            if (r<prob_door)
                xn(4) = 1.0;
                dHandle.FaceColor = 'none';
                dTextHandle.String = 'Door open';
                door_open_count=door_open_count+1;
            else
                xn(4) = 0.0;
                dHandle.FaceColor = [0.7,0.7,0.7];
                dTextHandle.String = 'Door closed';
            end
        elseif (x(4)==0.0 && door_open_count>=bound)
            xn(4) = 0.0;
            dHandle.FaceColor = [0.7,0.7,0.7];
            dTextHandle.String = 'Door closed';
        else % when the door is opened, it remains open until the robot reaches the kitchen
            if (atKitchen(x)==1)
                xn(4) = 0.0;
                dHandle.FaceColor = [0.7,0.7,0.7];
                dTextHandle.String = 'Door closed';
            else
                xn(4) = 1.0;
                dHandle.FaceColor = 'none';
                dTextHandle.String = 'Door open';
            end
        end
    end

end



