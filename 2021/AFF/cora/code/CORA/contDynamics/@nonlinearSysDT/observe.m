function [R,tcomp] = observe(obj,params,options)
% observe - computes the set of possible states of a set-based observer for
% nonlinear systems
%
% Syntax:  
%    [R,tcmp] = observe(obj,options)
%
% Inputs:
%    obj - continuous system object
%    params - model parameters
%    options - options for bounding the set of states
%
% Outputs:
%    R - set of possible states of the observer
%    tcomp - computation time
%
% Example: 
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: none

% Author:        Matthias Althoff
% Written:       25-Mar-2021
% Last update:   ---
% Last revision: ---


%------------- BEGIN CODE --------------

% options preprocessing
options = params2options(params,options);
options = checkOptionsObserve(obj,options);

% create vector of time points
tVec = options.tStart:options.timeStep:options.tFinal - options.timeStep;
tVec = tVec';

% compute symbolic derivatives
derivatives(obj,options);


% decide which observer to execute by options.alg
if strcmp(options.alg,'VolMin-A') 
    [R,tcomp] = observe_volMinA(obj, options);
elseif strcmp(options.alg,'VolMin-B') 
    [R,tcomp] = observe_volMinB(obj, options);
elseif strcmp(options.alg,'FRad-A') 
    [R,tcomp] = observe_FRadA(obj, options);
elseif strcmp(options.alg,'FRad-B') 
    [R,tcomp] = observe_FRadB(obj, options);
elseif strcmp(options.alg,'PRad-A') 
    [R,tcomp] = observe_PRadA(obj, options);
elseif strcmp(options.alg,'PRad-B') 
    [R,tcomp] = observe_PRadB(obj, options);
elseif strcmp(options.alg,'PRad-C-I') 
    [R,tcomp] = observe_PRadC_I(obj, options);
elseif strcmp(options.alg,'PRad-C-II') 
    [R,tcomp] = observe_PRadC_II(obj, options);
elseif strcmp(options.alg,'FRad-C') 
    [R,tcomp] = observe_FRadC(obj, options);
elseif strcmp(options.alg,'PRad-D') 
    [R,tcomp] = observe_PRadD(obj, options);
elseif strcmp(options.alg,'PRad-E') 
    [R,tcomp] = observe_PRadE(obj, options);
elseif strcmp(options.alg,'Nom-G') 
    [R,tcomp] = observe_NomG(obj, options);
elseif strcmp(options.alg,'Hinf-G') 
    [R,tcomp] = observe_HinfG(obj, options);
elseif strcmp(options.alg,'ESO-A')
    [R,tcomp] = observe_ESO_A(obj, options);
elseif strcmp(options.alg,'ESO-B')
    [R,tcomp] = observe_ESO_B(obj, options);
elseif strcmp(options.alg,'ESO-C')
    [R,tcomp] = observe_ESO_C(obj, options);    
elseif strcmp(options.alg,'ESO-D')
    [R,tcomp] = observe_ESO_D(obj, options);
elseif strcmp(options.alg,'CZN-A')
    [R,tcomp] = observe_CZN_A(obj, options);  
elseif strcmp(options.alg,'CZN-B')
    [R,tcomp] = observe_CZN_B(obj, options);  
elseif strcmp(options.alg,'backward')
    [R,tcomp] = observe_backward(obj,options);
elseif strcmp(options.alg,'ROPO')
    [R,tcomp] = observe_ROPO(obj,options);
end


% create object of class reachSet
R = createReachSetObject(R,tVec,options);

end

% Auxiliary Functions -----------------------------------------------------

function R = createReachSetObject(R,tVec,options)
% create and object of class reachSet that stores the reachable set

    % time-point reachable set
    timePoint.set = R;
    timePoint.time = num2cell(tVec);
    
    if length(timePoint.time) ~= length(timePoint.set)
       timePoint.time = timePoint.time(1:length(timePoint.set)); 
    end
    
    % time interval reachable set (for plotting reasons, the time point set 
    % is also stored as a time interval set)
    timeInt.set = R;
    timeInt.time = cell(length(R),1);
    
    for i = 1:length(R)
        timeInt.time{i} = interval(tVec(i),tVec(i) + options.timeStep);
    end
    
    % construct object of class reachSet
    R = reachSet(timePoint, timeInt);
end


%------------- END OF CODE --------------