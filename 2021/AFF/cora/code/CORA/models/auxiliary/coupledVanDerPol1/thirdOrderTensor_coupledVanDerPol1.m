function [Tf,ind] = thirdOrderTensor_coupledVanDerPol1(x,u)



 Tf{1,1} = interval(sparse(5,5),sparse(5,5));



 Tf{1,2} = interval(sparse(5,5),sparse(5,5));



 Tf{1,3} = interval(sparse(5,5),sparse(5,5));



 Tf{1,4} = interval(sparse(5,5),sparse(5,5));



 Tf{1,5} = interval(sparse(5,5),sparse(5,5));



 Tf{2,1} = interval(sparse(5,5),sparse(5,5));

Tf{2,1}(2,1) = -2;
Tf{2,1}(1,2) = -2;


 Tf{2,2} = interval(sparse(5,5),sparse(5,5));

Tf{2,2}(1,1) = -2;


 Tf{2,3} = interval(sparse(5,5),sparse(5,5));



 Tf{2,4} = interval(sparse(5,5),sparse(5,5));



 Tf{2,5} = interval(sparse(5,5),sparse(5,5));



 Tf{3,1} = interval(sparse(5,5),sparse(5,5));



 Tf{3,2} = interval(sparse(5,5),sparse(5,5));



 Tf{3,3} = interval(sparse(5,5),sparse(5,5));



 Tf{3,4} = interval(sparse(5,5),sparse(5,5));



 Tf{3,5} = interval(sparse(5,5),sparse(5,5));



 Tf{4,1} = interval(sparse(5,5),sparse(5,5));



 Tf{4,2} = interval(sparse(5,5),sparse(5,5));



 Tf{4,3} = interval(sparse(5,5),sparse(5,5));

Tf{4,3}(4,3) = -2;
Tf{4,3}(3,4) = -2;


 Tf{4,4} = interval(sparse(5,5),sparse(5,5));

Tf{4,4}(3,3) = -2;


 Tf{4,5} = interval(sparse(5,5),sparse(5,5));


 ind = cell(4,1);
 ind{1} = [];


 ind{2} = [1;2];


 ind{3} = [];


 ind{4} = [3;4];

