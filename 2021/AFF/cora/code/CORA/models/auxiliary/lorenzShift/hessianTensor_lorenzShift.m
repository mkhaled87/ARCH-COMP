function Hf=hessianTensor_lorenzShift(x,u)



 Hf{1} = sparse(4,4);



 Hf{2} = sparse(4,4);

Hf{2}(3,1) = -1;
Hf{2}(1,3) = -1;


 Hf{3} = sparse(4,4);

Hf{3}(2,1) = 1;
Hf{3}(1,2) = 1;
