function res = in(obj1,obj2)
% in - determines if obj2 is fully inside the set of strips represented by
% obj1
%
% Syntax:  
%    res = in(obj1,obj2)
%
% Inputs:
%    obj1 - strips object
%    obj2 - contSet object
%
% Outputs:
%    res - 1 if obj2 is contained in obj1, 0 otherwise
%
% Example: 
%    S = strips([-2 1],1,1);
%    zono1 = zonotope([3 1 1 0;3 1 0 1]);
%    zono2 = zono1 - [3;3];
%
%    in(hs,zono1)
%    in(hs,zono2)
%
%    figure
%    hold on
%    xlim([-6,6]);
%    ylim([-6,6]);
%    plot(hs,[1,2],'b');
%    plot(zono1,[1,2],'g','Filled',true,'EdgeColor','none');
%
%    figure
%    hold on
%    xlim([-6,6]);
%    ylim([-6,6]);
%    plot(hs,[1,2],'b');
%    plot(zono2,[1,2],'r','Filled',true,'EdgeColor','none');
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: zonotope/in

% Author:       Matthias Althoff
% Written:      08-Mar-2021
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------

%init result
res = 0;

% point in halfspace containment
if isnumeric(obj2)
    res = all(abs(obj1.C * obj2 - obj1.y) <= obj1.d);

% set in halfspace containment
else
    % loop over all halfspaces
    for i = 1:size(obj1.C,1)
        % upper bound
        val = supportFunc(obj2,obj1.C(i,:),'upper');
        if (val - obj1.y) > obj1.d
            return
        end
        % lower bound
        val = supportFunc(obj2,obj1.C(i,:),'lower');
        if (val - obj1.y) < -obj1.d
            return
        end
        % obj2 is enclosed
        res = 1;
    end
end

%------------- END OF CODE --------------