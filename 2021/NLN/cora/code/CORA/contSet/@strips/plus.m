function obj = plus(obj,S)
% plus - Overloaded '+' operator for the Minkowski addition of two
%    a strip with another set
%
% Syntax:  
%    obj = plus(obj,S)
%
% Inputs:
%    obj - strips object 
%    S - contSet object or numerical vector
%
% Outputs:
%    obj - strips object after Minkowski addition
%
% Example: 
%    S = strips([1,1],1);
%    zono = zonotope([1.5;0],eye(2));
%
%    res = S + zono;
%
%    figure; hold on;
%    xlim([-5;5]); ylim([-5;5]);
%    plot(S,[1,2],'r');
%    plot(res,[1,2],'b');
%    xlim([-4;4]); ylim([-4;4]);
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also: mtimes

% Author:       Niklas Kochdumper
% Written:      23-November-2020 
% Last update:  ---
% Last revision:---

%------------- BEGIN CODE --------------

    % determine strips object
    if ~isa(obj,'strips')
        temp = S;
        S = obj;
        obj = temp;
    end
    
    % compute interval enclosure
    int = interval(obj.C*S);
    
    % construct resulting strips object
    d = obj.d + rad(int);
    y = obj.y + center(int);
    
    obj = strips(obj.C,d,y);


%------------- END OF CODE --------------