function [A,B]=jacobian_brusselator(x,u)

A=[2*x(1)*x(2) - 5/2,x(1)^2;...
3/2 - 2*x(1)*x(2),-x(1)^2];

B=[0;...
0];

